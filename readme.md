Laravel GDPR Shield
===================
[![pipeline status](https://gitlab.com/judahnator/gdpr-shield/badges/master/pipeline.svg)](https://gitlab.com/judahnator/gdpr-shield/commits/master)

Do you not want to care about GDPR compliance? Well this is the package for you!

Installation Instructions
-------------------------
Require this package via composer.

`composer require judahnator/gdpr-shield`

If you are using Laravel 5.6+, Laravel will automatically pick up the required service provider. If you are using Laravel 5.5 or lower you will need to add this service provider to your `config/app.php` file manually.

```php
// ...
\judahnator\GdprShield\ServiceProvider::class,
// ...
```

Configuration
-------------
Run `php artisan vendor:publish` to publish the configuration file for this package. You will find it at `config/gdpr.php` once that is done. 

There are a few items there that you can customize.

* abort_message:
  * This is what is displayed to the user if they attempt to access the site from the EU.
* geolocation_driver:
  * The driver class to use for general geolocation lookups. By default we use a driver that polls geoplugin.net and caches the result, but if your app sees a lot of diverse traffic you may want to look into other drivers.
* geolite_database_path:
  * If you choose to use the GeoLite2 database driver, you will need to provide the path to that database file here.
  * You can find the database download [here](https://dev.maxmind.com/geoip/geoip2/geolite2/), be sure to pay attention to their licencing agreement.


Usage
-----
Everything is fairly turn-key. If this package is installed then all EU traffic will be given a 403 error with the defined error message, all other traffic will see the site like normal.