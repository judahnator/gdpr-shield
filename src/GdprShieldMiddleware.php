<?php

namespace judahnator\GdprShield;


use Closure;
use judahnator\GdprShield\Geolocation\GeoLocationDriverInterface;
use judahnator\GdprShield\Geolocation\GeoPluginDriver;

class GdprShieldMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $className = config('gdpr.geolocation_driver', GeoPluginDriver::class);
        $GeolocationDriver = new $className;

        if (!$GeolocationDriver instanceof GeoLocationDriverInterface) {
            throw new \RuntimeException('Invalid geolocation driver provided.');
        }

        if ($GeolocationDriver->ipInEu($request->ip())) {
            abort(403, config('gdpr.abort_message', 'Due to GDPR compliance issues, we will not serve EU customers'));
        }

        return $next($request);
    }
}
