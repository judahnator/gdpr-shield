<?php

namespace judahnator\GdprShield\Geolocation;


use GeoIp2\Database\Reader;

class GeoIP2GeolocationDriver implements GeoLocationDriverInterface
{
    public function ipInEu(string $ipAddress): bool
    {
        return (new Reader(config('gdpr.geolite_database_path')))
            ->country($ipAddress)
            ->country
            ->isInEuropeanUnion;
    }
}