<?php

namespace judahnator\GdprShield\Geolocation;


interface GeoLocationDriverInterface
{

    /**
     * Given an ip address, return the two letter all uppercase country code it belongs to.
     *
     * @param string $ipAddress
     * @return bool
     */
    public function ipInEu(string $ipAddress): bool;

}