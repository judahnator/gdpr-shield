<?php

namespace judahnator\GdprShield\Geolocation;

use Illuminate\Support\Facades\Cache;


/**
 * Class GeoPluginDriver
 *
 * This driver queries the http://www.geoplugin.net site for your geolocation queries.
 *
 * @package judahnator\GdprShield\Geolocation
 */
class GeoPluginDriver implements GeoLocationDriverInterface
{
    public function ipInEu(string $ipAddress): bool
    {
        return Cache::remember(
            sha1("ip_{$ipAddress}_country"),
            3600,
            function() use ($ipAddress) {
                return json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip={$ipAddress}"))->geoplugin_inEU;
            }
        );
    }
}