<?php

namespace judahnator\GdprShield;


use Illuminate\Support\ServiceProvider as BaseServiceProvider;


class ServiceProvider extends BaseServiceProvider
{

    public function boot()
    {
        app('router')->pushMiddlewareToGroup('web', GdprShieldMiddleware::class);
        $this->publishes([
            __DIR__.'/config.php' => config_path('gdpr.php')
        ]);
    }

}