<?php

namespace judahnator\GdprShield\Tests\BrowserTests;


use Illuminate\Http\Request;
use judahnator\GdprShield\GdprShieldMiddleware;
use judahnator\GdprShield\Tests\TestCase;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BrowserTest extends TestCase
{

    public function testMiddlewareSuccess(): void
    {
        $this->assertEquals('', static::sendTestRequest('128.101.101.101'), 'Unexpected request return value.');
    }

    public function testMiddlewareFail(): void
    {
        try {
            static::sendTestRequest('2.20.182.1');
            $this->fail('Middleware did not block an EU ip address.');
        } catch (HttpException $e) {
            $this->assertEquals(403, $e->getStatusCode());
            $this->assertEquals(config('gdpr.abort_message'), $e->getMessage());
        }
    }

    protected static function sendTestRequest(string $remote_addr): string
    {
        return (new GdprShieldMiddleware())
            ->handle(
                Request::create(
                    '/',
                    'GET',
                    [],
                    [],
                    [],
                    [
                        'REMOTE_ADDR' => $remote_addr
                    ]
                ),
                function (): string
                {
                    return '';
                }
            );
    }

}