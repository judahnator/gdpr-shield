<?php

namespace judahnator\GdprShield\Tests\GeoLocationDriverTests;


use judahnator\GdprShield\Geolocation\GeoLocationDriverInterface;
use judahnator\GdprShield\Tests\TestCase;

abstract class DriverTestCase extends TestCase
{

    abstract protected static function getDriver(): GeoLocationDriverInterface;

    public function testAssertingCountryInEuOrNot(): void
    {
        $driver = static::getDriver();

        $this->assertFalse(
            $driver->ipInEu('128.101.101.101'), // US IP
            "The ".get_class($driver)." driver failed asserting that an ip was not in the EU"
        );

        $this->assertTrue(
            $driver->ipInEu('2.20.182.1'), // AT IP
            "The ".get_class($driver)." driver failed asserting that an ip was in the EU"
        );
    }

}