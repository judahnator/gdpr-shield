<?php

namespace judahnator\GdprShield\Tests\GeoLocationDriverTests;


use judahnator\GdprShield\Geolocation\GeoIP2GeolocationDriver;
use judahnator\GdprShield\Geolocation\GeoLocationDriverInterface;

class GeoIP2DriverTest extends DriverTestCase
{

    protected static function getDriver(): GeoLocationDriverInterface
    {
        return new GeoIP2GeolocationDriver();
    }

    protected function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);
        $app['config']->set('gdpr.geolite_database_path', __DIR__.'/../../GeoLite2-Country.mmdb');
    }

    protected function setUp()
    {
        parent::setUp();
        if (!file_exists(config('gdpr.geolite_database_path'))) {
            $this->markTestSkipped('No GeoLite2 database found, cannot run test for this driver.');
        }
    }

}