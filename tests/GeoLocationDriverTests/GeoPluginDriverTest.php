<?php

namespace judahnator\GdprShield\Tests\GeoLocationDriverTests;


use judahnator\GdprShield\Geolocation\GeoLocationDriverInterface;
use judahnator\GdprShield\Geolocation\GeoPluginDriver;

class GeoPluginDriverTest extends DriverTestCase
{
    protected static function getDriver(): GeoLocationDriverInterface
    {
        return new GeoPluginDriver();
    }
}