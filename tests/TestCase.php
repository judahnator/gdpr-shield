<?php

namespace judahnator\GdprShield\Tests;


use judahnator\GdprShield\ServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('gdpr', require __DIR__.'/../src/config.php');
    }

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }

}